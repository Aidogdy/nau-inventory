import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';
import { get } from 'lodash';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;
  snack: MatSnackBarRef<SimpleSnackBar>;


  username: string;
  password: string;

  constructor(
    private builder: FormBuilder,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.form = this.builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

    Object.values(this.form.controls).forEach(field => {
      if (field.value) {
        field.updateValueAndValidity();
        field.markAsDirty();
      }
    });
  }

  ngOnDestroy() {
    if (this.snack) {
      this.snack.dismiss();
    }
  }

  submit() {
    const data = this.form.value;
    console.log(data);
  }

}
