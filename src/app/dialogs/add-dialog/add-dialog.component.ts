import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { DataService } from '../../services/data-service';
import { Device } from '../../models/device';
import { LoaderService } from '../../services/loader-service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})

export class AddDialogComponent {

  startDate = new Date(1990, 0, 1);

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Device,
    public dataService: DataService,
    private loaderService: LoaderService,
    private snackBar: MatSnackBar) {
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  onNoClick() {
    this.dialogRef.close();
  }

  confirmAdd() {
    this.loaderService.show();
    delete this.data['device'];
    this.dataService.addDevice(this.data).subscribe((data) => {
      this.showSnackBar("Device succesfully added", "");
      this.loaderService.hide();
    }, (error) => {
      this.showSnackBar("Error:" + error, "");
      this.loaderService.hide();
    });
  }

  showSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
