import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { DataService } from '../../services/data-service';
import { LoaderService } from '../../services/loader-service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {

  private snack: MatSnackBarRef<SimpleSnackBar>;

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.snack) {
      this.snack.dismiss();
    }
  }

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dataService: DataService,
    private loaderService: LoaderService,
    private snackBar: MatSnackBar
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete() {
    this.loaderService.show();

    this.dataService.deleteDevice(this.data.id).subscribe((data) => {
      this.snackBar.open('Device succesfully deleted', '', { duration: 2000 });
      this.loaderService.hide();
    }, (error) => {
      this.snackBar.open('Error: ' + error, '', { duration: 2000 });
      this.loaderService.hide();
    });
  }

}
