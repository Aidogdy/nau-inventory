import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Device } from '../models/device';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable()
export class DataService {
  private readonly API_URL = 'http://localhost:8080';

  dataChange: BehaviorSubject<Device[]> = new BehaviorSubject<Device[]>([]);
  dialogData: any;

  constructor(private http: HttpClient) { }

  get data(): Device[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  public getDevices() {
    return this.http.get<Device[]>(this.API_URL + '/items');
  }

  public addDevice(device: Device) {
    return this.http.post<Device>(this.API_URL + '/items', device, httpOptions);
  }

  public updateDevice(device) {
    return this.http.put(this.API_URL + '/items', device, httpOptions);
  }

  public deleteDevice(id: number) {
    return this.http.delete(this.API_URL + "/items/" + id);
  }


}
