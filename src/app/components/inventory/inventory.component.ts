import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenav, MatTableDataSource, MatSort, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { DataService } from '../../services/data-service';
import { Device } from '../../models/device';
import { AddDialogComponent } from '../../dialogs/add-dialog/add-dialog.component';
import { EditDialogComponent } from '../../dialogs/edit-dialog/edit-dialog.component';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { LoaderService } from '../../services/loader-service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})

export class InventoryComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['id', 'name', 'os', 'model', 'description', 'actions'];
  devices: MatTableDataSource<Device>;
  isLoading = false;
  dataLength = 0;

  opened = true;
  events = [];

  menuItems = [
    { name: "Computers", url: "/inventory", component: "", icon: "desktop_mac" },
    { name: "Printers", url: "/login", component: "", icon: "print" },
    { name: "Projectors", url: "/", component: "", icon: "tonality" },
    { name: "Accessories", url: "/", component: "", icon: "device_hub" }
  ]

  constructor(public dialog: MatDialog,
    public dataService: DataService,
    private snackBar: MatSnackBar,
    private loaderService: LoaderService) {
  }

  ngAfterViewInit() {
    this.isLoading = true;
    this.dataService.getDevices().subscribe((data) => {
      this.isLoading = false;
      this.devices = new MatTableDataSource(data);

      this.devices.sort = this.sort;
      this.devices.paginator = this.paginator;
      this.dataLength = data.length;
    }, (error) => {
      this.isLoading = false
    });
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.devices.filter = filterValue.trim().toLowerCase();
  }

  addNew(device: Device) {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: { device: {} }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.dataService.addDevice(this.dataService.getDialogData());
      }
    });
  }

  startEdit(id: number, name: string, model: string, os: string, description: string) {
    const dv: Device = { id: id, name: name, model: model, os: os, description: description, user_id: 0 };
    const dialogRef = this.dialog.open(EditDialogComponent, { data: dv });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loaderService.show();
        this.dataService.updateDevice(this.dataService.getDialogData()).subscribe((data) => {
          this.showSnackBar("Device succesfully updated", "");
          this.loaderService.hide();
        }, (error) => {
          this.showSnackBar("Error: " + error, "");
          this.loaderService.hide();
        });;
      }
    });
  }

  deleteItem(id: number, name: string): void {
    console.log(id, name);
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { name: name, id: id }
    });

    // this.dataService.deleteDevice(id)
    //   .subscribe(data => {
    //     // this.devices = this.devices.filter(u => u.id !== id);
    //   })
  };

  showSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
