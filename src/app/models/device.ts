export interface Device {
  id: number;
  name: string;
  os: string;
  model: string;
  description: string;
  user_id: number;
}
